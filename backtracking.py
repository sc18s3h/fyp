import time
from utility import printing

# Function that checks if a cell is valid
def valid(x,y,n,board):
    #check row and column
    for i in range(9):
        if board[x][i] == n or board[i][y] == n:
            return False

    #check box
    new_x = x//3 * 3
    new_y = y//3 * 3
    for i in range(3):
        for j in range(3):
            if board[new_x + i][new_y + j] == n:
                return False

    return True

# Function that looks for an empty cell (board[i][j]== 0)
def find_empty_cell(board):
    for i in range(9):
        for j in range(9):
            if board[i][j] == 0:
                return (i,j)

    return (-1,-1)

# Brute Force approach with backtracking
def brute(board):
    (x, y) = find_empty_cell(board)
    if (x, y) == (-1, -1):
        return board

    # Values assignment
    for i in range(1,10):
        if valid(x,y,i,board):
            board[x][y] = i
            if brute(board):
                return board
            board[x][y] = 0

def brute_solve(board):
    start_time = time.time()
    printing(board)
    print("________________________")
    printing(brute(board))
    print("________________________")
    print("--- %s seconds ---" % (time.time() - start_time))
