## Code based on Peter Norvig's python2 sudoku solver

## Constraint propagation combined with backtracking

## See http://norvig.com/sudoku.html

## Throughout this program we have:
##   r is a row,    e.g. 'A'
##   c is a column, e.g. '3'
##   s is a square, e.g. 'A3'
##   d is a digit,  e.g. '9'
##   u is a unit,   e.g. ['A1','B1','C1','D1','E1','F1','G1','H1','I1']
##   grid is a grid,e.g. 81 non-blank chars, e.g. starting with '.18...7...
##   values is a dict of possible values, e.g. {'A1':'12349', 'A2':'8', ...}


import time
from utility import printing, readFile


def cross(A, B):
    "Cross product of elements in A and elements in B."
    return [a+b for a in A for b in B]

digits   = '123456789'
rows     = 'ABCDEFGHI'
cols     = digits
squares  = cross(rows, cols)

# Column unit, row unit and box unit
unitlist = ([cross(rows, c) for c in cols] +
            [cross(r, cols) for r in rows] +
            [cross(rs, cs) for rs in ('ABC','DEF','GHI') for cs in ('123','456','789')])


units = {}
for s in squares:
    for u in unitlist:
        if s in u:
            if s not in units:
                units[s] = []
            units[s].append(u)

peers = {}
for s in squares:
    unit_set = set()
    for unit in units[s]:
        for square in unit:
            if square != s:
                unit_set.add(square)
    peers[s] = unit_set

# Convert grid into a dict with square as key and digit as value
def grid_values(grid):
    grid_chars = []
    for i in range(9):
        for j in range(9):
            x = grid[i][j]
            converted_x = str(x)
            grid_chars.append(converted_x)
    assert len(grid_chars) == 81
    return dict(zip(squares, grid_chars))

# Convert grid to a dict of possible values, {square: digits}, or
# return False if a contradiction is detected.
def parse_grid(grid):
    ## To start, every square can be any digit; then assign values from the grid.
    values = dict((s, digits) for s in squares)
    for s,d in grid_values(grid).items():
        if d in digits and not assign(values, s, d):
            return False ## (Fail if we can't assign d to square s.)
    return values

# Eliminate all the other values (except d) from values[s] and propagate.
# Return values, except return False if a contradiction is detected.
def assign(values, s, d):
    other_values = values[s].replace(d, '')
    if all(eliminate(values, s, d2) for d2 in other_values):
        return values
    else:
        return False

# Eliminate d from values[s]; propagate when values or places <= 2.
# Return values, except return False if a contradiction is detected.
def eliminate(values, s, d):
    if d not in values[s]:
        return values ## Already eliminated
    values[s] = values[s].replace(d,'')
    ## (1) If a square s is reduced to one value d2, then eliminate d2 from the peers.
    if len(values[s]) == 0:
        return False ## Contradiction: removed last value
    elif len(values[s]) == 1:
        d2 = values[s]
        if not all(eliminate(values, s2, d2) for s2 in peers[s]):
            return False
    ## (2) If a unit u is reduced to only one place for a value d, then put it there.
    for u in units[s]:
        dplaces = [s for s in u if d in values[s]]
        if len(dplaces) == 0:
            return False ## Contradiction: no place for this value
        elif len(dplaces) == 1:
            # d can only be in one place in unit; assign it there
            if not assign(values, dplaces[0], d):
                return False
    return values

# Using depth-first search and propagation, try all possible values.
def search(values):
    if values is False:
        return False ## Failed earlier
    if all(len(values[s]) == 1 for s in squares):
        return values ## Solved!
    ## Chose the unfilled square s with the fewest possibilities
    n,s = min((len(values[s]), s) for s in squares if len(values[s]) > 1)
    return some(search(assign(values.copy(), s, d)) for d in values[s])

# Return some element of seq that is true.
def some(seq):
    for e in seq:
        if e: return e
    return False

def solve(grid): return search(parse_grid(grid))

# Display these values as a 2-D grid.
def display(values):
    width = 1+max(len(values[s]) for s in squares)
    line = ''.join(['- '*width]*6)
    for r in rows:
        print(''.join(values[r+c].center(width)+(' |  ' if c in '36' else '') for c in cols))
        if r in 'CF':
            print(line )

def csp_solve(board):
    start_time = time.time()
    printing(board)
    print("________________________")
    display(solve(board))
    print("________________________")
    print("--- %s seconds ---" % (time.time() - start_time))
