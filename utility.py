import os

# A utility function to print grid
def printing(board):
	for i in range(9):
		if i % 3 == 0 and i != 0:
			print("- - - - - - - - - - - -  ")
		for j in range(9):
			if j % 3 == 0 and j != 0:
				print(" | ", end= " ")
			print(board[i][j], end = " ")
		print()

# A funtion that reads a txt file containing the grids
def readFile(fileName, n):
	with open(fileName, 'r') as file:
		all_file = file.read().strip()  # Read and remove any extra new line
		matrices = all_file.split('\n$\n')
		all_file_list = matrices[n].split('\n')  # make a list of lines
		final_data = [[int(each_int) for each_int in line.split()] for line in all_file_list]  # make list of list and convert to int
		return final_data

# A function that asks user for input and stores it in a 2D array
def own():
    board=[]
    print("\nPlease enter line by line your grid.")
    print("For example: 1 2 3 4 5 6 7 8 9\n")
    for i in range(9):
        print("Enter space separated elements of row {}".format(i))
        row=[int(x) for x in input().strip().split()]
        board.append(row)
    return board
