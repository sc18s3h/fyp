# Individual project 2020/2021
## Author: Sylvain Hu

## Build and run the code

* Python 3 or above
   * Follow instructions found here:  https://docs.python.org/3/using/unix.html#getting-and-installing-the-latest-version-of-python


To run the command-line interface:
    `$ python3 main.py'

When solver's screens shows, user is espected to input numbers such as '1'.
