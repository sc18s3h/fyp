#!/usr/bin/python

import os
import sys
from utility import printing, readFile, own
from backtracking import brute_solve
from csp import csp_solve
from exact import exact_solve
from hypersudoku_backtracking import h_brute_solve
from hypersudoku_csp import h_csp_solve

def show_screen():
	print("\nSudoku Solver")
	print("\nOptions:")
	print("   Type your own Sudoku, please type 1")
	print("   Solve a written Sudoku, please type 2")
	print("   Exit the solver, please type 3\n")

def show_options():
    print("\nSudoku Algorithms")
    print("\nOptions:")
    print("   For Bruteforce Standard Sudoku, please type 1")
    print("   For Constraint Propagation Standard Sudoku, please type 2")
    print("   For Exact Cover Standard Sudoku, please type 3")
    print("   For Bruteforce Hyper Sudoku, please type 4")
    print("   For Constraint Propagation Hyper Sudoku, please type 5")

def show_difficulties():
	print("\nSudoku Difficulty")
	print("\nOptions:")
	print("   For easy, please type 1")
	print("   For hard, please type 2\n")

def continue_grid():
	print("\nNext Grid?")
	print("\nOptions:")
	print("   Next grid, please type 1")
	print("   Exit, please type 2\n")

def goodbye():
    print("\nNo more grids, goodbye!")


def main():
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    easy = os.path.join(fileDir, 'grids/easy.txt')
    hard = os.path.join(fileDir, 'grids/hard.txt')
    h_easy = os.path.join(fileDir, 'grids/h_easy.txt')
    h_hard = os.path.join(fileDir, 'grids/h_hard.txt')
    message = "Please enter a number: "
    show_screen()
    var = int(input(message))
    count = 0
    # Print command line arguments
    if var == 1:
        show_options()
        var = int(input(message))
		# Brute Force
        if var == 1:
            brute_solve(own())

		# CSP
        elif var == 2:
            csp_solve(own())

		# Exact Cover
        elif var == 3:
            exact_solve(own())

		# H Brute Force
        elif var == 4:
            h_brute_solve(own())

		# H CSP
        elif var == 5:
            h_csp_solve(own())

        else:
            sys.exit("Invalid input!")

	# Solve sudoku in database
    elif var == 2:
        show_options()
        var = int(input(message))

		# Brute Force
        if var == 1:
            show_difficulties()
            var = int(input(message))
            if var == 1:
                brute_solve(readFile(easy, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        brute_solve(readFile(easy, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

            elif var == 2:
                brute_solve(readFile(hard, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        brute_solve(readFile(hard, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

		# CSP
        elif var == 2:
            show_difficulties()
            var = int(input(message))
            if var == 1:
                csp_solve(readFile(easy, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        csp_solve(readFile(easy, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

            elif var == 2:
                csp_solve(readFile(hard, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        csp_solve(readFile(hard, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

		# Exact Cover
        elif var == 3:
            show_difficulties()
            var = int(input(message))
            if var == 1:
                exact_solve(readFile(easy, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        exact_solve(readFile(easy, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

            elif var == 2:
                exact_solve(readFile(hard, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        exact_solve(readFile(hard, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

		# H Brute Force
        elif var == 4:
            show_difficulties()
            var = int(input(message))
            if var == 1:
                h_brute_solve(readFile(h_easy, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        h_brute_solve(readFile(h_easy, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

            elif var == 2:
                h_brute_solve(readFile(h_hard, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        h_brute_solve(readFile(h_hard, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

		# H CSP
        elif var == 5:
            show_difficulties()
            var = int(input(message))
            if var == 1:
                h_csp_solve(readFile(h_easy, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        h_csp_solve(readFile(h_easy, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

            elif var == 2:
                h_csp_solve(readFile(h_hard, count))
                for x in range(1, 10):
                    continue_grid()
                    var = int(input(message))
                    if var == 1:
                        count += 1
                        h_csp_solve(readFile(h_hard, count))
                    elif var == 2:
                        sys.exit(0)
                goodbye()

	# Exit
    elif var == 3:
        print("See you soon!")
        sys.exit(0)

    else:
        sys.exit("Invalid input!")

if __name__ == "__main__":
    main()
