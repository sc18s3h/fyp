#!/usr/bin/env python3

## Code based on Ali Assaf's algorithm x and sudoku solver
## https://www.cs.mcgill.ca/~aassaf9/python/algorithm_x.html
## Author: Ali Assaf <ali.assaf.mail@gmail.com>
## Copyright: (C) 2010 Ali Assaf
## License: GNU General Public License <http://www.gnu.org/licenses/>

from itertools import product
import time
from utility import printing


# Sudoku as an exact cover problem
def solve_sudoku(grid):
    N = len(grid)

    # Four constraint set:
    # rc: Row-Column (e.g. constraint for row 0 col 0 is labelled ('rc', (0, 0)))
    # rn: Row-Number (e.g. constraint for row 0 num 1 is labelled ('rn', (0, 1)))
    # cn: Column-Number (e.g. constraint for col 0 num 1 is labelled ('cn', (0, 1)))
    # bn: Row-Number (e.g. constraint for box 0 num 1 is labelled ('bn', (0, 1)))
    X = ([("rc", rc) for rc in product(range(N), range(N))] +
         [("rn", rn) for rn in product(range(N), range(1, N + 1))] +
         [("cn", cn) for cn in product(range(N), range(1, N + 1))] +
         [("bn", bn) for bn in product(range(N), range(1, N + 1))])

    # All possibilities (e.g. key(8,8,9) represent the cell in the last row and
    # column with the number 9, R9C9#9 in standard sudoku representation, R8C8#9
    # here because first row and column are labelled as 0. Value represents the
    # constraints of the key, for key(8,8,9), the four constraints are ('rc',(8,
    # 8)), ('rn', (8,9)), ('cn',(8,9)) and ('bn', (8,9)), these are the same
    # constraints in list X)
    Y = dict()
    for r, c, n in product(range(N), range(N), range(1, N + 1)):
        b = (r // 3) * 3 + (c // 3) # Box number
        Y[(r, c, n)] = [
            ("rc", (r, c)),
            ("rn", (r, n)),
            ("cn", (c, n)),
            ("bn", (b, n))]

    # Changes X to a dict and represent the sudoku as an exact cover problem.
    # Constraint now contains all the related possibilities (e.g. for constraint
    # ('rc', (0, 0)) as the key, it has 9 possibilities which are the values of
    # that key: (0,0,1),(0,0,2),(0,0,3),(0,0,4),(0,0,5),(0,0,6),(0,0,7),(0,0,8),
    # (0,0,9). It means that for the cell in the first row and column, its
    # number can be anything between 1 and 9, where the 9 possibilities. In a
    # standard representation it would be : R1C1 = ({R1C1#1}, {R1C1#2}, {R1C1#3},
    # {R1C1#4}, {R1C1#5}, {R1C1#6}, {R1C1#7}, {R1C1#8}, {R1C1#9})
    X, Y = exact_cover(X, Y)

    for i, row in enumerate(grid):
        for j, n in enumerate(row):
            if n:
                select(X, Y, (i, j, n))

    for solution in solve(X, Y, []):
        for (r, c, n) in solution:
            grid[r][c] = n

    yield grid

def exact_cover(X, Y):
    X = {j: set() for j in X}
    for i, row in Y.items():
        for j in row:
            X[j].add(i)
    return X, Y

# Algorithm X
def solve(X, Y, solution):
    if not X:
        yield list(solution)
    else:
        c = min(X, key=lambda c: len(X[c]))
        for r in list(X[c]):
            solution.append(r)
            cols = select(X, Y, r)
            for s in solve(X, Y, solution):
                yield s
            deselect(X, Y, r, cols)
            solution.pop()

def select(X, Y, r):
    cols = []
    for j in Y[r]:
        for i in X[j]:
            for k in Y[i]:
                if k != j:
                    X[k].remove(i)
        cols.append(X.pop(j))
    return cols

def deselect(X, Y, r, cols):
    for j in reversed(Y[r]):
        X[j] = cols.pop()
        for i in X[j]:
            for k in Y[i]:
                if k != j:
                    X[k].add(i)

def exact_solve(board):
    start_time = time.time()
    printing(board)
    print("________________________")
    for solution in solve_sudoku(board):
        printing(solution)
    print("________________________")
    print("--- %s seconds ---" % (time.time() - start_time))
